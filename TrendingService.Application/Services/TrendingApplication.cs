﻿using AutoMapper;
using System.Xml.Linq;
using TrendingService.Application.DTOs;
using TrendingService.Application.Interfaces;
using TrendingService.Domain.Common;
using TrendingService.Domain.Entities;
using TrendingService.Domain.Interfaces;

namespace TrendingService.Application.Services
{
    public class TrendingApplication : ITrendingApplication
    {
        private readonly ITrendingRepository _trendingRepository;
        private readonly IMapper _mapper;

        public TrendingApplication(ITrendingRepository trendingRepository, IMapper mapper)
        {
            _trendingRepository = trendingRepository;
            _mapper = mapper;
        }

        public ResponseModel SaveTrendingMovies(List<TrendingDTO> listTrending)
        {
            if (listTrending == null || !listTrending.Any())
            {
                return new ResponseModel { Success = false, Message = "No trending movies to save." };
            }

            try
            {                
                string movieXml = ConvertMoviesToXml(listTrending);
                bool Trending = true;
                bool Popular = false;

                ResponseModel response = _trendingRepository.SaveTrendingMovies(movieXml,Trending,Popular);

                return response;
            }
            catch (Exception ex)
            {
                return new ResponseModel { Success = false, Message = "An error occurred while saving trending movies." + ex.Message };
            }        
        }

        private string ConvertMoviesToXml(List<TrendingDTO> movies)
        {
            var xElement = new XElement("ROOT",
                new XElement("Movies",
                    movies.Select(m => new XElement("Movie",
                        new XElement("Watchers", m.watchers),
                        new XElement("Title", m.movie.title),
                        new XElement("Year", m.movie.year),
                        new XElement("Trakt", m.movie.ids.trakt),
                        new XElement("Slug", m.movie.ids.slug),
                        new XElement("Imdb", m.movie.ids.imdb),
                        new XElement("Tmdb", m.movie.ids.tmdb)
                    ))
                )
            );

            return xElement.ToString();
        }

        public List<TrendingDTO> GetTrendingMovies()
        {
            try
            {
                ResponseModel<List<MoviesModel>> response = _trendingRepository.GetTrendingMovies();
                
                if (response.Success)
                { 
                    return _mapper.Map<List<TrendingDTO>>(response.Data);
                }
                else
                {                    
                    return new List<TrendingDTO>();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("An error occurred while getting trending movies.", ex);
            }
        }


    }
}
