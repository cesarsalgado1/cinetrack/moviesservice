﻿using TrendingService.Application.DTOs;
using TrendingService.Domain.Common;

namespace TrendingService.Application.Interfaces
{
    public interface ITrendingApplication
    {
        ResponseModel SaveTrendingMovies(List<TrendingDTO> trending);
        List<TrendingDTO> GetTrendingMovies();
    }
}
