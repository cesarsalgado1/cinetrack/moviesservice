﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendingService.Application.DTOs
{
    public class TrendingDTO
    {
        public int watchers { get; set; }
        public MovieDTO movie { get; set; }
    }
}
