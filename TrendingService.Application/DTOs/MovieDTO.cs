﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendingService.Application.DTOs
{
    public class MovieDTO
    {
        public string? title { get; set; }
        public int? year { get; set; }
        public MovieIdsDTO ids { get; set; }
    }
}
