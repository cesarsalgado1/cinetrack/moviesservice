﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SearchService.Application.DTOs;
using SearchService.Application.Interfaces;
using SearchService.Domain.Common;
using SearchService.WebApi.ViewModels;

namespace SearchService.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : Controller
    {
        private readonly ISearchApplication _searchApplication;
        public SearchController(ISearchApplication searchApplication) 
        {
            _searchApplication = searchApplication;
        }

        [HttpGet("SearchMovies")]
        public ActionResult<ResponseModel<List<MovieViewModel>>> SearchMovies(string searchTerm)
        {
            try
            {
                List<MovieDTO> listMovies = _searchApplication.SearchMovies(searchTerm);

                var movies = listMovies.Select(trend => new MovieViewModel
                {
                    Watchers = trend.Watchers,
                    Title = trend.Title,
                    Year = trend.Year,
                    Trakt = trend.Trakt,
                    Slug = trend.Slug,
                    Imdb = trend.Imdb,
                    Tmdb = trend.Tmdb,
                    classification = trend.classification
                }).ToList();

                return Ok(new ResponseModel<List<MovieViewModel>>
                {
                    Message = "Movies retrieved successfully",
                    Success = true,
                    Data = movies
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new ResponseModel<List<MovieViewModel>>
                {
                    Message = $"An error occurred while retrieving trending movies: {ex.Message}",
                    Success = false,
                    Data = null
                });
            }
        }


    }
}
