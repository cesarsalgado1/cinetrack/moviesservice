using SearchService.Application.Interfaces;
using SearchService.Application.Services;
using SearchService.Domain.Interfaces;
using SearchService.Infraestructure.Mappings;
using SearchService.Infraestructure.Persistence.Context;
using SearchService.Infraestructure.Persistence.Repositories;

var builder = WebApplication.CreateBuilder(args);
#region Interfaces   
builder.Services.AddScoped<ISearchApplication, SearchApplication>();
builder.Services.AddScoped<ISearchRepository, SearchRepository>();
builder.Services.AddSingleton<DapperContext>();
builder.Services.AddAutoMapper(typeof(MappingProfile));
#endregion
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


// Add CORS policy
builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowSpecificOriginTrending",
        builder => builder.WithOrigins("http://localhost:4200") // Permite solo solicitudes desde este origen
                                                                //.AllowAnyOrigin()       // Permitir�a todos los origenes (No recomendado para producci�n)
                          .AllowAnyMethod()         // Permite cualquier m�todo HTTP (GET, POST, PUT, DELETE, etc.)
                          .AllowAnyHeader());       // Permite cualquier encabezado HTTP en las solicitudes con CORS
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseDeveloperExceptionPage();
}

app.UseHttpsRedirection();

// Use CORS
app.UseCors("AllowSpecificOriginTrending");

app.UseAuthorization();

app.MapControllers();

app.Run();
