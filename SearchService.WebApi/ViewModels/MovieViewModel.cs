﻿namespace SearchService.WebApi.ViewModels
{
    public class MovieViewModel
    {
        public int Watchers { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public int Trakt { get; set; }
        public string Slug { get; set; }
        public string Imdb { get; set; }
        public int Tmdb { get; set; }
        public string classification { get; set; }
    }
}
