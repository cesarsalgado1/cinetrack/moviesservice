﻿using SearchService.Domain.Common;
using SearchService.Domain.Entities;

namespace SearchService.Domain.Interfaces
{
    public interface ISearchRepository
    {
        ResponseModel<List<MoviesModel>> SearchMovies(string searchTerm);
    }
}
