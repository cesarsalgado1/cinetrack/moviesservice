﻿using PopularService.Application.DTOs;
using PopularService.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PopularService.Application.Interfaces
{
    public interface IPopularApplication
    {
        ResponseModel SavePopularMovies(List<MovieDTO> listPopular);
        List<MovieDTO> GetPopularMovies();
    }
}
