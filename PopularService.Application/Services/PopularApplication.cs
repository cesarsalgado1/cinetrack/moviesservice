﻿using AutoMapper;
using PopularService.Application.DTOs;
using PopularService.Application.Interfaces;
using PopularService.Domain.Common;
using PopularService.Domain.Entities;
using PopularService.Domain.Interfaces;
using System.Xml.Linq;

namespace PopularService.Application.Services
{
    public class PopularApplication : IPopularApplication
    {
        private readonly IPopularRepository _popularRepository;
        private readonly IMapper _mapper;

        public PopularApplication(IPopularRepository trendingRepository, IMapper mapper)
        {
            _popularRepository = trendingRepository;
            _mapper = mapper;
        }

        public ResponseModel SavePopularMovies(List<MovieDTO> listPopular)
        {
            if (listPopular == null || !listPopular.Any())
            {
                return new ResponseModel { Success = false, Message = "No popular movies to save." };
            }

            try
            {
                string movieXml = ConvertMoviesToXml(listPopular);
                bool Trending = false;
                bool Popular = true;

                ResponseModel response = _popularRepository.SavePopularMovies(movieXml, Trending, Popular);

                return response;
            }
            catch (Exception ex)
            {
                return new ResponseModel { Success = false, Message = "An error occurred while saving popular movies." + ex.Message };
            }
        }

        private string ConvertMoviesToXml(List<MovieDTO> movies)
        {
            var xElement = new XElement("ROOT",
                new XElement("Movies",
                    movies.Select(m => new XElement("Movie",                        
                        new XElement("Title", m.title),
                        new XElement("Year", m.year),
                        new XElement("Trakt", m.ids.trakt),
                        new XElement("Slug", m.ids.slug),
                        new XElement("Imdb", m.ids.imdb),
                        new XElement("Tmdb", m.ids.tmdb)
                    ))
                )
            );

            return xElement.ToString();
        }

        public List<MovieDTO> GetPopularMovies()
        {
            try
            {
                ResponseModel<List<MoviesModel>> response = _popularRepository.GetPopularMovies();

                if (response.Success)
                {
                    return _mapper.Map<List<MovieDTO>>(response.Data);
                }
                else
                {
                    return new List<MovieDTO>();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("An error occurred while getting trending movies.", ex);
            }
        }


    }
}
