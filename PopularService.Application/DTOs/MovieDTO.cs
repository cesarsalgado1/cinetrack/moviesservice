﻿namespace PopularService.Application.DTOs
{
    public class MovieDTO
    {
        public string? title { get; set; }
        public int? year { get; set; }
        public MovieIdsDTO ids { get; set; }
    }
}
