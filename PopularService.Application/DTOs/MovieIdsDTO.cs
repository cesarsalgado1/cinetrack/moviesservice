﻿namespace PopularService.Application.DTOs
{
    public class MovieIdsDTO
    {
        public int? trakt { get; set; }
        public string? slug { get; set; }
        public string? imdb { get; set; }
        public int? tmdb { get; set; }
    }
}
