﻿using HomeService.Application.DTOs;
using HomeService.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeService.Application.Interfaces
{
    public interface IHomeApplication
    {
        ResponseModel<List<MoviesDTO>> GetRandomMovies();
    }
}
