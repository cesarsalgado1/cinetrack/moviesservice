﻿using AutoMapper;
using HomeService.Application.DTOs;
using HomeService.Application.Interfaces;
using HomeService.Domain.Common;
using HomeService.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeService.Application.Services
{
    public class HomeApplication : IHomeApplication
    {
        private readonly IHomeRepository _homeRepository;
        private readonly IMapper _mapper;
        public HomeApplication(IHomeRepository homeRepository, IMapper mapper)
        {
            _homeRepository = homeRepository;
            _mapper = mapper;
        }

        public ResponseModel<List<MoviesDTO>> GetRandomMovies()
        {
            try
            {
                var movies = _homeRepository.GetRandomMovies();
                if (movies.Success)
                {
                    var data = _mapper.Map<List<MoviesDTO>>(movies.Data);

                    return new ResponseModel<List<MoviesDTO>>()
                    {
                        Success = true,
                        Message = movies.Message,
                        Data = data
                    };
                }
                else
                {
                    return new ResponseModel<List<MoviesDTO>>()
                    {
                        Success = false,
                        Message = movies.Message,
                        Data = new List<MoviesDTO>()
                    };
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("An error occurred while getting random movies.", ex);
            }
        }

    }
}
