﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using TrendingService.Application.DTOs;
using TrendingService.Application.Interfaces;
using TrendingService.Domain.Common;
using TrendingService.WebApi.ViewModels;

namespace TrendingService.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendingController : Controller
    {
        private readonly ITrendingApplication _trendingApplication;

        public TrendingController(ITrendingApplication trendingApplication)
        {
            _trendingApplication = trendingApplication;
        }

        [HttpPost]
        [Route("SaveTrendingMovies")]
        public ActionResult<ResponseModel> SaveTrendingMovies(List<TrendingDTO> movies)
        {
            if (movies == null || !movies.Any())
            {
                return BadRequest(new ResponseModel
                {
                    Message = "The list of trending movies cannot be null or empty.",
                    Success = false
                });
            }

            try
            {
                ResponseModel response = _trendingApplication.SaveTrendingMovies(movies);

                if (response.Success)
                {
                    return Ok(response);
                }
                else
                {
                    return StatusCode(500, response);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new ResponseModel
                {
                    Message = $"An error occurred while saving trending movies: {ex.Message}",
                    Success = false
                });
            }
        }

        [HttpGet]
        [Route("GetTrendingMovies")]
        public ActionResult<ResponseModel<List<MoviesViewModel>>> GetTrendingMovies()
        {
            try
            {
                List<TrendingDTO> listTrending = _trendingApplication.GetTrendingMovies();
                
                var movies = listTrending.Select(trend => new MoviesViewModel
                {
                    watchers = trend.watchers,
                    title = trend.movie.title,
                    year = trend.movie.year,
                    ids_Trakt = trend.movie.ids.trakt,
                    ids_Slug = trend.movie.ids.slug,
                    ids_Imdb = trend.movie.ids.imdb,
                    ids_Tmdb = trend.movie.ids.tmdb
                }).ToList();

                return Ok(new ResponseModel<List<MoviesViewModel>>
                {
                    Message = "Trending movies retrieved successfully",
                    Success = true,
                    Data = movies
                });
            }
            catch (Exception ex)
            {                
                return StatusCode(500, new ResponseModel<List<MoviesViewModel>>
                {
                    Message = $"An error occurred while retrieving trending movies: {ex.Message}",
                    Success = false,
                    Data = null
                });
            }
        }



    }
}
