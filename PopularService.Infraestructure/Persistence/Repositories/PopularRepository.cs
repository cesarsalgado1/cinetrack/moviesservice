﻿using Dapper;
using PopularService.Domain.Common;
using PopularService.Domain.Entities;
using PopularService.Domain.Interfaces;
using PopularService.Infraestructure.Persistence.Context;
using System.Data;

namespace PopularService.Infraestructure.Persistence.Repositories
{
    public class PopularRepository : IPopularRepository
    {
        private readonly DapperContext _context;

        public PopularRepository(DapperContext context)
        {
            _context = context;
        }

        public ResponseModel SavePopularMovies(string movieXml, bool Trending, bool Popular)
        {
            try
            {
                string storedProcedure = "usp_SaveTrendingMovies";
                var parameters = new
                {
                    MovieXML = movieXml,
                    Trending = Trending,
                    Popular = Popular
                };
                using (var connection = _context.GetSQLConnection())
                {
                    return connection.QuerySingle<ResponseModel>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                return new ResponseModel
                {
                    Message = $"An unexpected error occurred: {ex.Message}",
                    Success = false,
                };
            }

        }


        public ResponseModel<List<MoviesModel>> GetPopularMovies()
        {
            try
            {
                string storedProcedure = "usp_GetPopularMovies";

                using (var connection = _context.GetSQLConnection())
                {
                    var respose = connection.Query<MoviesModel>(storedProcedure, commandType: CommandType.StoredProcedure).ToList();

                    return new ResponseModel<List<MoviesModel>>
                    {
                        Message = "Movies fetched successfully",
                        Success = true,
                        Data = respose
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseModel<List<MoviesModel>>
                {
                    Message = $"An error occurred while fetching trending movies: {ex.Message}",
                    Success = false,
                    Data = null
                };
            }
        }


    }
}
