﻿using AutoMapper;
using PopularService.Application.DTOs;
using PopularService.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace PopularService.Infraestructure.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<MovieDTO, MoviesModel>()                
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.title))
                .ForMember(dest => dest.Year, opt => opt.MapFrom(src => src.year))
                .ForPath(dest => dest.Trakt, opt => opt.MapFrom(src => src.ids.trakt))
                .ForPath(dest => dest.Slug, opt => opt.MapFrom(src => src.ids.slug))
                .ForPath(dest => dest.Imdb, opt => opt.MapFrom(src => src.ids.imdb))
                .ForPath(dest => dest.Tmdb, opt => opt.MapFrom(src => src.ids.tmdb));


            CreateMap<MoviesModel, MovieDTO>()
            .ForMember(dest => dest.title, opt => opt.MapFrom(src => src.Title))
            .ForMember(dest => dest.year, opt => opt.MapFrom(src => src.Year))
            .ForMember(dest => dest.ids, opt => opt.MapFrom(src => new MovieIdsDTO
            {
                trakt = src.Trakt,
                slug = src.Slug,
                imdb = src.Imdb,
                tmdb = src.Tmdb,
            }));



        }
    }
}
