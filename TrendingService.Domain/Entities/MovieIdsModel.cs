﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendingService.Domain.Entities
{
    public class MovieIdsModel
    {
        public int IdMovie_ids { get; set; }
        public int Trakt { get; set; }
        public string Slug { get; set; }
        public string Imdb { get; set; }
        public int Tmdb { get; set; }
    }
}
