﻿using TrendingService.Domain.Common;
using TrendingService.Domain.Entities;

namespace TrendingService.Domain.Interfaces
{
    public interface ITrendingRepository
    {
        ResponseModel SaveTrendingMovies(string movieXml,bool Trending,bool Popular);
        ResponseModel<List<MoviesModel>> GetTrendingMovies();
    }
}
