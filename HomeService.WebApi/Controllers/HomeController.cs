﻿using HomeService.Application.DTOs;
using HomeService.Application.Interfaces;
using HomeService.Domain.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HomeService.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IHomeApplication _homeApplication;
        public HomeController(IHomeApplication homeApplication)
        {
            _homeApplication = homeApplication;
        }


        [HttpGet("GetRandomMovies")]
        public ActionResult<ResponseModel<List<MoviesDTO>>> GetRandomMovies()
        {
            try
            {
                ResponseModel<List<MoviesDTO>> response = _homeApplication.GetRandomMovies();

                return response;
            }
            catch (Exception ex)
            {
                return StatusCode(500, new ResponseModel<List<MoviesDTO>>
                {
                    Message = $"An error occurred while retrieving trending movies: {ex.Message}",
                    Success = false,
                    Data = null
                });
            }
        }

    }
}
