﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendingService.Infraestructure.Persistence.Context
{
    public class DapperContext
    {
        //private readonly IConfiguration _config;
        private readonly string _pgSQLConnection;
        private readonly string _sqlConnection;

        public DapperContext(IConfiguration configuration)
        {
            //_config = configuration;
            _pgSQLConnection = configuration.GetConnectionString("conPSQL");
            _sqlConnection = configuration.GetConnectionString("conSQL");
        }

        public NpgsqlConnection GetPGSQLConnection() => new NpgsqlConnection(_pgSQLConnection);
        public SqlConnection GetSQLConnection() => new SqlConnection(_sqlConnection);
    }
}
