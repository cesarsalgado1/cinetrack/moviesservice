﻿using Dapper;
using System.Data;
using TrendingService.Domain.Common;
using TrendingService.Domain.Entities;
using TrendingService.Domain.Interfaces;
using TrendingService.Infraestructure.Persistence.Context;

namespace TrendingService.Infraestructure.Persistence.Repositories
{
    public class TrendingRepository : ITrendingRepository
    {
        private readonly DapperContext _context;
        
        public TrendingRepository(DapperContext context)
        {
            _context = context;
        }

        public ResponseModel SaveTrendingMovies(string movieXml, bool Trending, bool Popular)
        {
            try
            {
                string storedProcedure = "usp_SaveTrendingMovies";
                var parameters = new
                {
                    MovieXML = movieXml,
                    Trending = Trending,
                    Popular = Popular
                };
                using (var connection = _context.GetSQLConnection())
                {
                    return connection.QuerySingle<ResponseModel>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
                }

            }
            catch (Exception ex)
            {
                return new ResponseModel
                {
                    Message = $"An unexpected error occurred: {ex.Message}",
                    Success = false,
                };
            }
        }

        public ResponseModel<List<MoviesModel>> GetTrendingMovies()
        {
            try
            {
                string storedProcedure = "usp_GetTrendingMovies";

                using (var connection = _context.GetSQLConnection())
                {
                    var respose = connection.Query<MoviesModel>(storedProcedure, commandType: CommandType.StoredProcedure).ToList();

                    return new ResponseModel<List<MoviesModel>>
                    {
                        Message = "Movies fetched successfully",
                        Success = true,
                        Data = respose
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseModel<List<MoviesModel>>
                {
                    Message = $"An error occurred while fetching trending movies: {ex.Message}",
                    Success = false,
                    Data = null
                };
            }
        }


    }
}
