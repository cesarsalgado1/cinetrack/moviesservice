﻿using TrendingService.Application.DTOs;
using TrendingService.Domain.Entities;
using static System.Runtime.InteropServices.JavaScript.JSType;
using AutoMapper;

using System.Security.AccessControl;

namespace TrendingService.Infraestructure.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<TrendingDTO, MoviesModel>()
                .ForMember(dest => dest.Watchers, opt => opt.MapFrom(src => src.watchers))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.movie.title))
                .ForMember(dest => dest.Year, opt => opt.MapFrom(src => src.movie.year))
                .ForPath(dest => dest.Trakt, opt => opt.MapFrom(src => src.movie.ids.trakt))
                .ForPath(dest => dest.Slug, opt => opt.MapFrom(src => src.movie.ids.slug))
                .ForPath(dest => dest.Imdb, opt => opt.MapFrom(src => src.movie.ids.imdb))
                .ForPath(dest => dest.Tmdb, opt => opt.MapFrom(src => src.movie.ids.tmdb));


            CreateMap<MoviesModel, MovieDTO>()
            .ForMember(dest => dest.title, opt => opt.MapFrom(src => src.Title))
            .ForMember(dest => dest.year, opt => opt.MapFrom(src => src.Year))
            .ForMember(dest => dest.ids, opt => opt.MapFrom(src => new MovieIdsDTO
            {
                trakt = src.Trakt,
                slug =  src.Slug,
                imdb =  src.Imdb,
                tmdb =  src.Tmdb,
            }));

            CreateMap<MoviesModel, TrendingDTO>()
                .ForMember(dest => dest.watchers, opt => opt.MapFrom(src => src.Watchers))
                .ForMember(dest => dest.movie, opt => opt.MapFrom(src => src));

           
        }
    }
}
