﻿using Dapper;
using SearchService.Domain.Common;
using SearchService.Domain.Entities;
using SearchService.Domain.Interfaces;
using SearchService.Infraestructure.Persistence.Context;
using System.Data;

namespace SearchService.Infraestructure.Persistence.Repositories
{
    public class SearchRepository : ISearchRepository
    {
        private readonly DapperContext _context;

        public SearchRepository(DapperContext context)
        {
            _context = context;
        }


        public ResponseModel<List<MoviesModel>> SearchMovies(string searchTerm)
        {
            try
            {
                string storedProcedure = "usp_SearchTrendingAndPopularMovies";
                var parameters = new { searchTerm = searchTerm };
                using (var connection = _context.GetSQLConnection())
                {
                    var respose = connection.Query<MoviesModel>(storedProcedure, parameters, commandType: CommandType.StoredProcedure).ToList();

                    return new ResponseModel<List<MoviesModel>>
                    {
                        Message = "Movies fetched successfully",
                        Success = true,
                        Data = respose
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseModel<List<MoviesModel>>
                {
                    Message = $"An unexpected error occurred: {ex.Message}",
                    Success = false,
                    Data = null
                };
            }
        }



    }
}
