﻿using AutoMapper;
using SearchService.Application.DTOs;
using SearchService.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace SearchService.Infraestructure.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<MoviesModel, MovieDTO>();

            CreateMap<MovieDTO, MoviesModel>();
        }        
    }
}
