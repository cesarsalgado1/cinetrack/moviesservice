﻿using AutoMapper;
using SearchService.Application.DTOs;
using SearchService.Application.Interfaces;
using SearchService.Domain.Common;
using SearchService.Domain.Entities;
using SearchService.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchService.Application.Services
{
    public class SearchApplication : ISearchApplication
    {
        private readonly ISearchRepository _searchRepository;
        private readonly IMapper _mapper;

        public SearchApplication(ISearchRepository searchRepository, IMapper mapper)
        {
            _searchRepository = searchRepository;   
            _mapper = mapper;
        }


        public List<MovieDTO> SearchMovies(string searchTerm)
        {
            try
            {
                ResponseModel<List<MoviesModel>> response = _searchRepository.SearchMovies(searchTerm);
                if (response.Success)
                {
                    return _mapper.Map<List<MovieDTO>>(response.Data);
                }
                else
                {
                    return new List<MovieDTO>();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("An error occurred while getting trending movies.", ex);
            }
        }
    }
}
