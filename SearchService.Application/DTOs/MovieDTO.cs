﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchService.Application.DTOs
{
    public class MovieDTO
    {
        public int IdMovie { get; set; }
        public int Watchers { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public int Trakt { get; set; }
        public string Slug { get; set; }
        public string Imdb { get; set; }
        public int Tmdb { get; set; }
        public bool Trending { get; set; }
        public bool Popular { get; set; }
        public string classification
        {
            get
            {
                if (Trending)
                {
                    return "Trend";
                }
                else if (Popular)
                {
                    return "Popular";
                }
                else
                {
                    return string.Empty; // O cualquier valor por defecto que prefieras
                }
            }
        }
    }
}
