﻿using SearchService.Application.DTOs;
using SearchService.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchService.Application.Interfaces
{
    public interface ISearchApplication
    {
        List<MovieDTO> SearchMovies(string searchTerm);
    }
}
