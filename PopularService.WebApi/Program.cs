using PopularService.Application.Interfaces;
using PopularService.Application.Services;
using PopularService.Domain.Interfaces;
using PopularService.Infraestructure.Mappings;
using PopularService.Infraestructure.Persistence.Context;
using PopularService.Infraestructure.Persistence.Repositories;

var builder = WebApplication.CreateBuilder(args);

#region Interfaces      
builder.Services.AddScoped<IPopularApplication, PopularApplication>();
builder.Services.AddScoped<IPopularRepository, PopularRepository>();
builder.Services.AddSingleton<DapperContext>();
builder.Services.AddAutoMapper(typeof(MappingProfile));

#endregion

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Add CORS policy
builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowSpecificOriginPopular",
        builder => builder.WithOrigins("http://localhost:4200") // Permite solo solicitudes desde este origen
                          //.AllowAnyOrigin()       // Permitir�a todos los origenes (No recomendado para producci�n)
                          .AllowAnyMethod()         // Permite cualquier m�todo HTTP (GET, POST, PUT, DELETE, etc.)
                          .AllowAnyHeader());       // Permite cualquier encabezado HTTP en las solicitudes con CORS
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseDeveloperExceptionPage();
}

app.UseHttpsRedirection();

// Use CORS
app.UseCors("AllowSpecificOriginPopular");

app.UseAuthorization();

app.MapControllers();

app.Run();
