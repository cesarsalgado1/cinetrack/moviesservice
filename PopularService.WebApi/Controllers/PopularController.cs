﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PopularService.Application.DTOs;
using PopularService.Application.Interfaces;
using PopularService.Domain.Common;
using PopularService.WebApi.ViewModels;

namespace PopularService.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PopularController : ControllerBase
    {
        private readonly IPopularApplication _popularApplication;

        public PopularController(IPopularApplication  popularApplication)
        {
            _popularApplication = popularApplication;
        }

        [HttpPost]
        [Route("SavePopularMovies")]
        public ActionResult<ResponseModel> SavePopularMovies(List<MovieDTO> movies)
        {
            if (movies == null || !movies.Any())
            {
                return BadRequest(new ResponseModel
                {
                    Message = "The list of popular movies cannot be null or empty.",
                    Success = false
                });
            }

            try
            {
                ResponseModel response = _popularApplication.SavePopularMovies(movies);

                if (response.Success)
                {
                    return Ok(response);
                }
                else
                {
                    return StatusCode(500, response);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new ResponseModel
                {
                    Message = $"An error occurred while saving popular movies: {ex.Message}",
                    Success = false
                });
            }
        }


        [HttpGet]
        [Route("GetPopularMovies")]
        public ActionResult<ResponseModel<List<MoviesViewModel>>> GetTrendingMovies()
        {
            try
            {
                List<MovieDTO> listTrending = _popularApplication.GetPopularMovies();

                var movies = listTrending.Select(trend => new MoviesViewModel
                {                    
                    title = trend.title,
                    year = trend.year,
                    ids_Trakt = trend.ids.trakt,
                    ids_Slug = trend.ids.slug,
                    ids_Imdb = trend.ids.imdb,
                    ids_Tmdb = trend.ids.tmdb
                }).ToList();

                return Ok(new ResponseModel<List<MoviesViewModel>>
                {
                    Message = "Popular movies retrieved successfully",
                    Success = true,
                    Data = movies
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new ResponseModel<List<MoviesViewModel>>
                {
                    Message = $"An error occurred while retrieving popular movies: {ex.Message}",
                    Success = false,
                    Data = null
                });
            }
        }



    }
}
