﻿using HomeService.Domain.Common;
using HomeService.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeService.Domain.Interfaces
{
    public interface IHomeRepository
    {
        ResponseModel<List<MoviesModel>> GetRandomMovies();
    }
}
