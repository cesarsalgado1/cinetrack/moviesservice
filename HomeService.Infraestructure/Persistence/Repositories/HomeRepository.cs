﻿using Dapper;
using HomeService.Domain.Common;
using HomeService.Domain.Entities;
using HomeService.Domain.Interfaces;
using HomeService.Infraestructure.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeService.Infraestructure.Persistence.Repositories
{
    public class HomeRepository : IHomeRepository
    {
        private readonly DapperContext _context;

        public HomeRepository(DapperContext context)
        {
            _context = context;
        }

        public ResponseModel<List<MoviesModel>> GetRandomMovies()
        {
            try
            {
                string storedProcedure = "usp_GetRandomMovies";
                
                using (var connection = _context.GetSQLConnection())
                {
                    var respose = connection.Query<MoviesModel>(storedProcedure, commandType: CommandType.StoredProcedure).ToList();

                    return new ResponseModel<List<MoviesModel>>
                    {
                        Message = "Movies fetched successfully",
                        Success = true,
                        Data = respose
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseModel<List<MoviesModel>>
                {
                    Message = $"An unexpected error occurred: {ex.Message}",
                    Success = false,
                    Data = null
                };
            }
        }

    }
}
