﻿using AutoMapper;
using HomeService.Application.DTOs;
using HomeService.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeService.Infraestructure.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<MoviesModel, MoviesDTO>();

            CreateMap<MoviesDTO, MoviesModel>();
        }
    }
}
