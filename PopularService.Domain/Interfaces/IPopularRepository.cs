﻿using PopularService.Domain.Common;
using PopularService.Domain.Entities;

namespace PopularService.Domain.Interfaces
{
    public interface IPopularRepository
    {
        ResponseModel SavePopularMovies(string movieXml, bool Trending, bool Popular);
        ResponseModel<List<MoviesModel>> GetPopularMovies();
    }
}
